# Rust port of C++ [ssloy/tinyraytracer](https://github.com/ssloy/tinyraytracer)

Demo project for self-study only. Porting of C++ code to Rust.

Simple ray tracer with spheres and floor-like surface.


## Run:
```sh
cargo run --release
```
Then open out.ppm in current directory.