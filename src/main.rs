use std::io::prelude::*;
use std::io::BufWriter;
use std::vec;

mod geom;
mod renderer;
use renderer::prelude::*;

use geom::{Vec3, Vec4};

// Used for debug tracing on sample point.
#[allow(dead_code)]
fn test_render(spheres: &vec::Vec<Sphere>, lights: &vec::Vec<Light>) -> std::io::Result<()> {
    let width = 1024;
    let height = 768;
    let fov = std::f32::consts::PI / 3.;
    let i = 562;
    let j = 384;
    let dir_x = (i as f32 + 0.5) - width as f32 / 2.0;
    let dir_y = -(j as f32 + 0.5) + height as f32 / 2.0;
    let dir_z = -(height as f32) / (2.0 * (fov / 2.0).tan());

    let v = cast_ray(
        &Vec3::zero(),
        &Vec3(dir_x, dir_y, dir_z).normalize(),
        spheres,
        lights,
        0,
    );
    println!("{} {} {}\n", v.0, v.1, v.2);
    Ok(())
}

fn write_image(width: usize, height: usize, framebuffer: &[Vec3]) -> std::io::Result<()> {
    let f = std::fs::File::create("./out.ppm")?;
    let mut buf = BufWriter::new(f);
    buf.write(format!("P6\n{} {}\n255\n", width, height).as_bytes())?;
    for c in framebuffer {
        let mut v = *c;
        let max = c.0.max(c.1.max(c.2));
        if max > 1.0 {
            v = v * (1. / max);
        }
        buf.write(&[
            (255 as f32 * v.0) as u8,
            (255 as f32 * v.1) as u8,
            (255 as f32 * v.2) as u8,
        ])?;
    }
    Ok(())
}

fn main() -> std::io::Result<()> {
    let ivory = Material {
        refractive_index: 1.0,
        albedo: Vec4 {
            0: [0.6, 0.3, 0.1, 0.0],
        },
        diffuse_color: Vec3(0.4, 0.4, 0.3),
        specular_exponent: 50.,
    };
    let glass = Material {
        refractive_index: 1.5,
        albedo: Vec4 {
            0: [0.0, 0.5, 0.1, 0.8],
        },
        diffuse_color: Vec3(0.6, 0.7, 0.8),
        specular_exponent: 125.,
    };
    let red_rubber = Material {
        refractive_index: 1.0,
        albedo: Vec4 {
            0: [0.9, 0.1, 0.0, 0.0],
        },
        diffuse_color: Vec3(0.3, 0.1, 0.1),
        specular_exponent: 10.,
    };
    let mirror = Material {
        refractive_index: 1.0,
        albedo: Vec4 {
            0: [0.0, 10.0, 0.8, 0.0],
        },
        diffuse_color: Vec3(1.0, 1.0, 1.0),
        specular_exponent: 1425.,
    };
    let spheres = vec![
        Sphere::new(Vec3(-3.0, 0.0, -16.0), 2.0, ivory),
        Sphere::new(Vec3(-1.0, -1.5, -12.0), 2.0, glass),
        Sphere::new(Vec3(1.5, -0.5, -18.0), 3.0, red_rubber),
        Sphere::new(Vec3(7., 5., -18.0), 4.0, mirror),
    ];

    let lights = vec![
        Light {
            position: Vec3(-20., 20., 20.),
            intensity: 1.5,
        },
        Light {
            position: Vec3(30., 50., -25.),
            intensity: 1.8,
        },
        Light {
            position: Vec3(30., 20., 30.),
            intensity: 1.7,
        },
    ];

    let width = 1024;
    let height = 768;
    let framebuffer = render(width, height, &spheres, &lights);
    write_image(width, height, &framebuffer)?;
    // test_render(&spheres, &lights)?;
    Ok(())
}
