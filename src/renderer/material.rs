use crate::geom::{Vec3, Vec4};

#[derive(Copy, Clone, Debug)]
pub struct Material {
    pub refractive_index: f32,
    pub albedo: Vec4,
    pub diffuse_color: Vec3,
    pub specular_exponent: f32,
}

impl Material {
    pub fn new() -> Self {
        Self {
            refractive_index: 1f32,
            albedo: Vec4 {
                0: [1f32, 0f32, 0f32, 0f32],
            },
            diffuse_color: Vec3::zero(),
            specular_exponent: 0f32,
        }
    }
}
