use crate::geom::Vec3;

#[derive(Copy, Clone, Debug)]
pub struct Light {
    pub position: Vec3,
    pub intensity: f32,
}