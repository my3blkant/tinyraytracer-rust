use super::material::Material;
use crate::geom::Vec3;

pub struct Sphere {
    pub center: Vec3,
    pub radius: f32,
    pub material: Material,
    pub radius2: f32,
}

impl Sphere {
    pub fn new(center: Vec3, radius: f32, material: Material) -> Self {
        Sphere {
            center: center,
            radius: radius,
            material: material,
            radius2: radius * radius,
        }
    }

    pub fn ray_intersect(&self, orig: &Vec3, dir: &Vec3, t0: &mut f32) -> bool {
        let l = self.center - *orig;
        let tca = l * *dir;
        let d2 = l * l - tca * tca;
        let r2 = self.radius2;
        if d2 > r2 {
            return false;
        }
        let thc = (r2 - d2).sqrt();
        *t0 = tca - thc;
        let t1 = tca + thc;
        if *t0 < 1e-3 {
            *t0 = t1;
        }
        if *t0 < 1e-3 {
            return false;
        }
        return true;
    }
}
