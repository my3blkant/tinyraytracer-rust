use super::light::Light;
use super::material::Material;
use super::rays;
use super::sphere::Sphere;
use crate::geom::Vec3;
use std::vec::Vec;

pub fn intersect(
    orig: &Vec3,
    dir: &Vec3,
    spheres: &[Sphere],
    hit: &mut Vec3,
    n: &mut Vec3,
    material: &mut Material,
) -> bool {
    let mut spheres_dist = f32::MAX;
    for sphere in spheres.iter() {
        let mut dist_i: f32 = 0.0;
        if sphere.ray_intersect(orig, dir, &mut dist_i) && dist_i < spheres_dist {
            spheres_dist = dist_i;
            *hit = *orig + (*dir) * dist_i;
            *n = (*hit - sphere.center).normalize();
            *material = sphere.material;
        }
    }
    let mut checkerboard_dist = f32::MAX;
    if dir.1.abs() > 1e-3 {
        let d = -(orig.1 + 4.0) / dir.1;
        let pt = *orig + *dir * d;
        if d > 1e-3 && pt.0.abs() < 10.0 && pt.2 < -10.0 && pt.2 > -30.0 && d < spheres_dist {
            checkerboard_dist = d;
            *hit = pt;
            *n = Vec3::y(1.);
            if ((hit.0 * 0.5 + 1000.0).floor() as i32 + (hit.2 * 0.5).floor() as i32) & 1 != 0 {
                material.diffuse_color = Vec3(0.3, 0.3, 0.3);
            } else {
                material.diffuse_color = Vec3(0.3, 0.2, 0.1);
            }
        }
    }

    return spheres_dist.min(checkerboard_dist) < 1000.0;
}

pub fn render(width: usize, height: usize, spheres: &[Sphere], lights: &[Light]) -> Vec<Vec3> {
    let fov = std::f32::consts::PI / 3.;
    let mut framebuffer = Vec::<Vec3>::with_capacity(width * height);
    framebuffer.resize(width * height, Vec3::zero());

    for j in 0..height {
        for i in 0..width {
            let dir_x = (i as f32 + 0.5) - width as f32 / 2.0;
            let dir_y = -(j as f32 + 0.5) + height as f32 / 2.0;
            let dir_z = -(height as f32) / (2.0 * (fov / 2.0).tan());
            let r = rays::cast_ray(
                &Vec3::zero(),
                &Vec3(dir_x, dir_y, dir_z).normalize(),
                spheres,
                lights,
                0,
            );
            framebuffer[i + j * width] = r;
        }
    }
    return framebuffer;
}
