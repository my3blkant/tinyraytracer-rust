use super::light::Light;
use super::material::Material;
use super::sphere::Sphere;
use crate::geom::Vec3;

use super::scene;

pub fn reflect(i: &Vec3, n: &Vec3) -> Vec3 {
    *i - (*n) * 2f32 * ((*i) * (*n))
}

pub fn refract(i: &Vec3, n: &Vec3, eta_t: f32, eta_i: f32) -> Vec3 {
    let cosi = -(1f32).min((*i) * (*n)).max(-1.0);
    if cosi < 0.0 {
        return refract(i, &-*n, eta_i, eta_t);
    }
    let eta = eta_i / eta_t;
    let k = 1.0 - eta.powi(2) * (1.0 - cosi.powi(2));

    if k < 0.0 {
        Vec3::x(1.)
    } else {
        *i * eta + *n * (eta * cosi - k.sqrt())
    }
}

pub fn cast_ray(orig: &Vec3, dir: &Vec3, spheres: &[Sphere], lights: &[Light], depth: u32) -> Vec3 {
    let mut point: Vec3 = Vec3::zero();
    let mut n: Vec3 = Vec3::zero();
    let mut material = Material::new();

    if depth > 4 || !scene::intersect(orig, dir, spheres, &mut point, &mut n, &mut material) {
        return Vec3(0.2, 0.7, 0.8);
    }
    let reflect_dir = reflect(dir, &n).normalize();
    let refract_dir = refract(dir, &n, material.refractive_index, 1.0).normalize();
    let reflect_color = cast_ray(&point, &reflect_dir, spheres, lights, depth + 1);
    let refract_color = cast_ray(&point, &refract_dir, spheres, lights, depth + 1);
    let mut diffuse_light_intensity: f32 = 0.0;
    let mut specular_light_intensity: f32 = 0.0;

    for light in lights {
        let light_dir = (light.position - point).normalize();
        let mut shadow_pt = Vec3::zero();
        let mut trashnrm = Vec3::zero();
        let mut trashmat = Material::new();

        if scene::intersect(
            &point,
            &light_dir,
            spheres,
            &mut shadow_pt,
            &mut trashnrm,
            &mut trashmat,
        ) && (shadow_pt - point).norm() < (light.position - point).norm()
        {
            continue;
        }
        diffuse_light_intensity += light.intensity * (light_dir * n).max(0.0);
        specular_light_intensity += (-reflect(&-light_dir, &n) * *dir)
            .max(0.0)
            .powf(material.specular_exponent)
            * light.intensity;
    }

    return material.diffuse_color * diffuse_light_intensity * material.albedo.0[0]
        + Vec3::one() * specular_light_intensity * material.albedo.0[1]
        + reflect_color * material.albedo.0[2]
        + refract_color * material.albedo.0[3];
}
