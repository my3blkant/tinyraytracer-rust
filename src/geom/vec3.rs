use std::ops::{Add, Mul, Neg, Sub};

#[derive(Clone, Debug, Copy)]
pub struct Vec3(pub f32, pub f32, pub f32);

impl Neg for Vec3 {
    type Output = Vec3;
    fn neg(self) -> Self {
        self * (-1.0)
    }
}

impl Sub for Vec3 {
    type Output = Vec3;
    #[inline]
    fn sub(self, other: Self) -> Self {
        Self(self.0 - other.0, self.1 - other.1, self.2 - other.2)
    }
}

impl Add for Vec3 {
    type Output = Vec3;
    #[inline]
    fn add(self, other: Self) -> Self {
        Self(self.0 + other.0, self.1 + other.1, self.2 + other.2)
    }
}

impl Mul for Vec3 {
    type Output = f32;
    #[inline]
    fn mul(self, other: Self) -> f32 {
        self.0 * other.0 + self.1 * other.1 + self.2 * other.2
    }
}

impl Mul<f32> for Vec3 {
    type Output = Vec3;
    #[inline]
    fn mul(self, other: f32) -> Self {
        Self(self.0 * other, self.1 * other, self.2 * other)
    }
}

impl Vec3 {
    pub fn x(x: f32) -> Self {
        Self(x, 0., 0.)
    }
    pub fn y(y: f32) -> Self {
        Self(0., y, 0.)
    }
    pub fn zero() -> Self {
        Self(0., 0., 0.)
    }
    pub fn one() -> Self {
        Self(1., 1., 1.)
    }
    pub fn norm(&self) -> f32 {
        (self.0.powi(2) + self.1.powi(2) + self.2.powi(2)).sqrt()
    }

    pub fn normalize(self) -> Self {
        self.normalize_l(1.0)
    }

    pub fn normalize_l(self, l: f32) -> Self {
        let n = l / self.norm();
        self * n
    }

    pub fn cross(self, other: Self) -> Self {
        Self(
            self.1 * other.2 - self.2 * other.1,
            self.2 * other.0 - self.0 * other.2,
            self.0 * other.1 - self.1 * other.0,
        )
    }
}
