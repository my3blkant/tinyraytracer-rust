// That generic Vec was implemented before Vec3, but for performance reason Vec3 is actually used.
use std::ops::{Add, Mul, Neg, Sub};

mod vec3;
pub use vec3::Vec3;

#[derive(Clone, Debug, Copy)]
pub struct Vec<const DIM: usize>(pub [f32; DIM]);

impl<const T: usize> Neg for Vec<T> {
    type Output = Vec<T>;
    fn neg(self) -> Self {
        self * (-1.0)
    }
}

impl<const T: usize> Sub for Vec<T> {
    type Output = Vec<T>;
    fn sub(self, other: Self) -> Self {
        let mut v = Vec::<T>([0.0; T]);
        for i in 0..T {
            v.0[i] = self.0[i] - other.0[i];
        }
        v
    }
}

impl<const T: usize> Add for Vec<T> {
    type Output = Vec<T>;
    fn add(self, other: Self) -> Self {
        let mut v = Vec::<T>([0.0; T]);
        for i in 0..T {
            v.0[i] = self.0[i] + other.0[i];
        }
        v
    }
}

impl<const T: usize> Mul for Vec<T> {
    type Output = f32;
    fn mul(self, other: Self) -> f32 {
        let mut sum = 0.0;
        for i in 0..T {
            sum += self.0[i] * other.0[i];
        }
        sum
    }
}

impl<const T: usize> Mul<f32> for Vec<T> {
    type Output = Vec<T>;
    fn mul(self, other: f32) -> Self {
        let mut v = Vec::<T>([0.0; T]);
        for i in 0..T {
            v.0[i] = self.0[i] * other;
        }
        v
    }
}

// pub type Vec3 = Vec<3>;
pub type Vec4 = Vec<4>;

impl<const T: usize> Vec<T> {
    pub fn zero() -> Self {
        Self { 0: [0.0; T] }
    }
    pub fn one() -> Self {
        Self { 0: [1.0; T] }
    }
    pub fn norm(&self) -> f32 {
        let mut ssum = 0.0;
        for x in &self.0 {
            ssum += x.powi(2);
        }
        ssum.sqrt()
    }
    pub fn normalize(self) -> Self {
        self.normalize_l(1.0)
    }
    pub fn normalize_l(self, l: f32) -> Self {
        let n = l / self.norm();
        self * n
    }
}

impl Vec<3> {
    pub fn cross(self, other: Self) -> Self {
        Self {
            0: [
                self.0[1] * other.0[2] - self.0[2] * other.0[1],
                self.0[2] * other.0[0] - self.0[0] * other.0[2],
                self.0[0] * other.0[1] - self.0[1] * other.0[0],
            ],
        }
    }
}
